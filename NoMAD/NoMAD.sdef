<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE dictionary SYSTEM "file://localhost/System/Library/DTDs/sdef.dtd">

<dictionary title="ScriptTest Terminology">

  <suite name="Standard Suite" code="nMaD" description="Common classes and commands for all applications.">

    <command name="count" code="corecnte" description="Return the number of elements of a particular class within an object.">
      <cocoa class="NSCountCommand"/>
      <access-group identifier="*"/>
      <direct-parameter type="specifier" requires-access="r" description="The objects to be counted."/>
      <parameter name="each" code="kocl" type="type" optional="yes" description="The class of objects to be counted." hidden="yes">
        <cocoa key="ObjectClass"/>
      </parameter>
      <result type="integer" description="The count."/>
    </command>

    <command name="delete" code="coredelo" description="Delete an object.">
      <cocoa class="NSDeleteCommand"/>
      <access-group identifier="*"/>
      <direct-parameter type="specifier" description="The object(s) to delete."/>
    </command>

    <command name="duplicate" code="coreclon" description="Copy an object.">
      <cocoa class="NSCloneCommand"/>
      <access-group identifier="*"/>
      <direct-parameter type="specifier" requires-access="r" description="The object(s) to copy."/>
      <parameter name="to" code="insh" type="location specifier" description="The location for the new copy or copies." optional="yes">
        <cocoa key="ToLocation"/>
      </parameter>
      <parameter name="with properties" code="prdt" type="record" description="Properties to set in the new copy or copies right away." optional="yes">
        <cocoa key="WithProperties"/>
      </parameter>
    </command>

    <command name="exists" code="coredoex" description="Verify that an object exists.">
      <cocoa class="NSExistsCommand"/>
      <access-group identifier="*"/>
      <direct-parameter type="any" requires-access="r" description="The object(s) to check."/>
      <result type="boolean" description="Did the object(s) exist?"/>
    </command>

    <command name="make" code="corecrel" description="Create a new object.">
      <cocoa class="NSCreateCommand"/>
      <access-group identifier="*"/>
      <parameter name="new" code="kocl" type="type" description="The class of the new object.">
        <cocoa key="ObjectClass"/>
      </parameter>
      <parameter name="at" code="insh" type="location specifier" optional="yes" description="The location at which to insert the object.">
        <cocoa key="Location"/>
      </parameter>
      <parameter name="with data" code="data" type="any" optional="yes" description="The initial contents of the object.">
        <cocoa key="ObjectData"/>
      </parameter>
      <parameter name="with properties" code="prdt" type="record" optional="yes" description="The initial values for properties of the object.">
        <cocoa key="KeyDictionary"/>
      </parameter>
      <result type="specifier" description="The new object."/>
    </command>

    <class name="application" code="capp" description="The application's top-level scripting object.">
      <cocoa class="NSApplication"/>
      <property name="name" code="pnam" type="text" access="r" description="The name of the application."/>
      <property name="frontmost" code="pisf" type="boolean" access="r" description="Is this the active application?">
        <cocoa key="isActive"/>
      </property>
      <property name="version" code="vers" type="text" access="r" description="The version number of the application."/>
      <element type="window" access="r">
        <cocoa key="orderedWindows"/>
      </element>
      <responds-to command="open">
        <cocoa method="handleOpenScriptCommand:"/>
      </responds-to>
      <responds-to command="quit">
        <cocoa method="handleQuitScriptCommand:"/>
      </responds-to>
    </class>

    <class name="window" code="cwin" description="A window.">
      <cocoa class="NSWindow"/>
      <property name="name" code="pnam" type="text" access="r" description="The title of the window.">
        <cocoa key="title"/>
      </property>
      <property name="id" code="ID  " type="integer" access="r" description="The unique identifier of the window.">
        <cocoa key="uniqueID"/>
      </property>
      <property name="index" code="pidx" type="integer" description="The index of the window, ordered front to back.">
        <cocoa key="orderedIndex"/>
      </property>
      <property name="bounds" code="pbnd" type="rectangle" description="The bounding rectangle of the window.">
        <cocoa key="boundsAsQDRect"/>
      </property>
      <property name="closeable" code="hclb" type="boolean" access="r" description="Does the window have a close button?">
        <cocoa key="hasCloseBox"/>
      </property>
      <property name="miniaturizable" code="ismn" type="boolean" access="r" description="Does the window have a minimize button?">
        <cocoa key="isMiniaturizable"/>
      </property>
      <property name="miniaturized" code="pmnd" type="boolean" description="Is the window minimized right now?">
        <cocoa key="isMiniaturized"/>
      </property>
      <property name="resizable" code="prsz" type="boolean" access="r" description="Can the window be resized?">
        <cocoa key="isResizable"/>
      </property>
      <property name="visible" code="pvis" type="boolean" description="Is the window visible right now?">
        <cocoa key="isVisible"/>
      </property>
      <property name="zoomable" code="iszm" type="boolean" access="r" description="Does the window have a zoom button?">
        <cocoa key="isZoomable"/>
      </property>
      <property name="zoomed" code="pzum" type="boolean" description="Is the window zoomed right now?">
        <cocoa key="isZoomed"/>
      </property>
    </class>
  </suite>
  <suite name="NoMAD Suite" code="nOMD" description="NoMAD-specific classes.">
      
      <class name="application" code="capp" description="NoMAD’s top level scripting object." plural="applications" inherits="application">
          <cocoa class="NSApplication"/>
          <property name="currentADUser" code="Idne" type="text" access="r"
              description="Current AD user.">
              <cocoa key="currentADUser"/>
          </property>
          <property name="currentADUserEmail" code="dmEl" type="text" access="r"
              description="Current AD user's email address.">
              <cocoa key="currentADUserEmail"/>
          </property>
          <property name="currentADDomain" code="dmEE" type="text" access="r"
              description="Current AD domain being used by NoMAD.">
              <cocoa key="currentADDomain"/>
          </property>
          <property name="currentADDomainController" code="deEl" type="text" access="r"
              description="Current AD domain controller being used by NoMAD.">
              <cocoa key="currentADDomainController"/>
          </property>
          <property name="currentADSite" code="XdEl" type="text" access="r"
              description="Current AD site in use.">
              <cocoa key="currentADSite"/>
          </property>
          <property name="signedIn" code="LekO" type="boolean" access="r"
              description="If the NoMAD user currently has Kerberos tickets.">
              <cocoa key="signedIn"/>
          </property>
          <property name="currentADUserExpiration" code="LekU" type="text" access="r"
              description="When the current AD user's password expires.">
              <cocoa key="currentADUserExpiration"/>
          </property>
          <property name="allPrefs" code="aPee" type="text" access="r"
              description="Current NoMAD preferences.">
              <cocoa key="allPrefs"/>
          </property>
      </class>
  </suite>
</dictionary>
